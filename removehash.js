const fs = require('fs')
const path = require('path')
const replace = require('replace-in-file');
const escapeRegExp = require('lodash.escaperegexp');

const sourceDir = './dist/'
const exportDir = './www_visitnewcastle/'


// store all files in custom arrays by type
let html = []
let js = []
let css = []
let maps = []
let staticAssets = []

removeHashfromName = (name) => {
  let extension = name.substring(name.lastIndexOf('.'), name.length)
  let hashStart = name.indexOf('.')
  let originalName = name.substring(0, hashStart)
  return originalName + extension
}

if (!fs.existsSync(exportDir)) {
  console.log('create export dir');
  fs.mkdirSync(exportDir);
}

fs.readdir(sourceDir, (err, files) => {

  files.forEach((file, index) => {
    // Dispatch files into arrays

    // first HTML files
    if (file.match(/.+\.(html)$/)) {
      console.log('html match', file)
      html.push(file)
    } else if (file.match(/.+\.(js)$/)) { // then JavaScripts
      js.push(file)
    } else if (file.match(/.+\.(map)$/)) { // then CSS
      maps.push(file)
    } else if (file.match(/.+\.(css)$/)) { // then sourcemaps
      css.push(file)
    } else if (file.match(/.+\..+$/)) { // all other files, exclude current directory and directory one level up
      staticAssets.push(file)
    }

    // Copy files with the new cleared out name
    fs.copyFile(sourceDir + file, exportDir + removeHashfromName(file), (err) => {
      if (err) throw err;
    });

  })
  // check what went where
  console.log('RESULT: html', html, 'css', css, 'js', js, 'staticAssets', staticAssets)

  // create an array for all compiled assets
  let assets = css.concat(js).concat(maps)

  // replace all other resources in html
  html.forEach(
    file => {
      staticAssets.forEach(name => {
        let options = {
          files: path.join(exportDir, file),
          from: new RegExp(escapeRegExp('/' + name), 'g'),
          to: (match) => '.' + removeHashfromName(match)
        }
        try {
          let changedFiles = replace.sync(options);
          // console.log('Modified file:', changedFiles);
        }
        catch (error) {
          console.error('Error occurred:', error);
        }
      })
      assets.forEach(name => {
        let options = {
          files: path.join(exportDir, file),
          from: new RegExp(escapeRegExp('/' + name), 'g'),
          to: (match) => '.' + removeHashfromName(match)
        }
        try {
          let changedFiles = replace.sync(options);
          // console.log('Modified file:', changedFiles);
        }
        catch (error) {
          console.error('Error occurred:', error);
        }
      })
      css.forEach(name => {
        let options = {
          files: path.join(exportDir, file),
          from: new RegExp(escapeRegExp('/' + name), 'g'),
          to: (match) => '.' + removeHashfromName(match)
        }
        try {
          let changedFiles = replace.sync(options);
          // console.log('Modified file:', changedFiles);
        }
        catch (error) {
          console.error('Error occurred:', error);
        }
      })
    })
  //replace map links in js
  js.forEach(
    file => {
      file = removeHashfromName(file)
      maps.forEach(name => {
        let options = {
          files: path.join(exportDir, file),
          from: new RegExp(escapeRegExp('/' + name), 'g'),
          to: (match) => '.' + removeHashfromName(match)
        }
        try {
          let changedFiles = replace.sync(options);
          // console.log('Modified files:', changedFiles);
        }
        catch (error) {
          console.error('Error occurred:', error);
        }
      })
    }
  )

  // replace links in css
  css.forEach(
    file => {
      file = removeHashfromName(file)
      console.log('css', file);

      staticAssets.forEach(name => {
        let options = {
          files: path.join(exportDir, file),
          from: new RegExp(escapeRegExp('/' + name), 'g'),
          to: (match) => '.' + removeHashfromName(match)
        }
        try {
          let changedFiles = replace.sync(options);
          console.log('Modified files:', changedFiles);
        }
        catch (error) {
          console.error('Error occurred:', error);
        }
      })
    }
  )
})
