import 'bootstrap';
// import './css/styles.scss'
import Tablesaw from 'tablesaw';

import $ from 'jquery';
import Vue from 'vue';
import axios from 'axios';
import SmoothScroll from 'smoothscroll-for-websites'

let vueListing;
let isGoogleMapReady = false;
window.vueListing = vueListing;
// SmoothScroll({ stepSize: 50 })
SmoothScroll({ animationTime: 600 });

// Google map
const initMap = function () {
    isGoogleMapReady = true;

    if (window.vueListing.listing.length > 0) {
        console.log("populateMap from initMap");
        populateMap(window.vueListing.filteredListing)
    }
}
window.initMap = initMap;

let populateMap = function (things) {
    let map, popup, Popup;
    let popupList = []

    let defaultLoc = { lat: -32.92774815965193, lng: 151.77214039470536 }
    map = new google.maps.Map(
        document.getElementById('map'), { zoom: 11, center: defaultLoc, streetViewControl: false });
    Popup = createPopupClass();

    // let popupContainer = document.getElementById('content');
    let popupContainer = document.createElement("div");
    popupContainer.setAttribute("id", "content");
    popupContainer.setAttribute("class", "map-card");
    document.getElementById("mapPopup").appendChild(popupContainer);

    things.forEach(item => {
        let loc = { lat: Number(item.boundary.split(",")[0]), lng: Number(item.boundary.split(",")[1]) }

        let marker = new google.maps.Marker({ optimized: false, position: loc, map: map });
        marker.addListener('click', function () {

            popupContainer.innerHTML = `<a href="${item.detailsUrl}"><img src="${item.image_src}" alt="Card image cap" class="card-img-top">
                <div class="card-body">
                  <h2 class="card-tag">${item.classificationsDescriptions}</h2> 
                  <h3 class="card-title">${item.name}</h3>
                  <p class="card-text d-none d-md-block">${item.description}</p>
                </div></a>`

            popup = new Popup(
                new google.maps.LatLng(loc.lat, loc.lng),
                popupContainer);
            popupList.push(popup)
            popup.setMap(map);
        });
    })

    google.maps.event.addListener(map, "click", function (event) {
        popup.setMap(null)
    });
}
window.populateMap = populateMap
/**
 * Returns the Popup class.
 *
 * Unfortunately, the Popup class can only be defined after
 * google.maps.OverlayView is defined, when the Maps API is loaded.
 * This function should be called by initMap.
 */
function createPopupClass() {
    /**
     * A customized popup on the map.
     * @param {!google.maps.LatLng} position
     * @param {!Element} content The bubble div.
     * @constructor
     * @extends {google.maps.OverlayView}
     */
    function Popup(position, content) {
        this.position = position;

        content.classList.add('popup-bubble');

        // This zero-height div is positioned at the bottom of the bubble.
        // var bubbleAnchor = document.createElement('div');
        // bubbleAnchor.classList.add('popup-bubble-anchor');
        // bubbleAnchor.appendChild(content);

        // This zero-height div is positioned at the bottom of the tip.
        this.containerDiv = document.createElement('div');
        this.containerDiv.classList.add('popup-container');
        this.containerDiv.appendChild(content);

        // Optionally stop clicks, etc., from bubbling up to the map.
        google.maps.OverlayView.preventMapHitsAndGesturesFrom(this.containerDiv);
    }
    // ES5 magic to extend google.maps.OverlayView.
    Popup.prototype = Object.create(google.maps.OverlayView.prototype);

    /** Called when the popup is added to the map. */
    Popup.prototype.onAdd = function () {
        this.getPanes().floatPane.appendChild(this.containerDiv);
    };

    /** Called when the popup is removed from the map. */
    Popup.prototype.onRemove = function () {
        if (this.containerDiv.parentElement) {
            this.containerDiv.parentElement.removeChild(this.containerDiv);
        }
    };

    /** Called each frame when the popup needs to draw itself. */
    Popup.prototype.draw = function () {
        var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);

        // Hide the popup when it is far out of view.
        var display =
            Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                'block' :
                'none';

        if (display === 'block') {
            this.containerDiv.style.left = divPosition.x + 'px';
            this.containerDiv.style.top = divPosition.y + 'px';
        }
        if (this.containerDiv.style.display !== display) {
            this.containerDiv.style.display = display;
        }
    };

    return Popup;
}


$(() => {
    console.log('JS Init', window);
    Tablesaw.init();

    let isDesktopMenu = false;
    let isDesktop;


    let showsticky = false;
    const stickyTrigger = 200;

    let showSearchbox = false;

    let isLanding = false;
    let isPageLocked = false;

    let checkViewportSize = function () {
        windowWidth = window.innerWidth;
        isDesktop = (windowWidth >= 992) ? true : false;
    }
    checkViewportSize();



    //

    // ----------------------------------------------------------------------------------------------------//
    // -------------------------------- ScrollLoc --------------------------------------------------------//
    // --------------------------------------------------------------------------------------------------//
    // Catch button events
    $(".toggle-pagelock").click(function (event) {
        console.log("toggle-pagelock clicked!!");
        togglePageLock()
    });
    // Fix: jQuery function called by Vue, as Vue delays DOM loading
    // $("#mapFullscreen").click(function (event) {
    //     console.log("click on #mapFullscreen!!!");

    //     togglePageLock()
    // });

    const togglePageLock = function () {
        // console.log("$('#mapPopup.show')[0]", $('#mapPopup.show')[0]);
        let isShowMap;
        if ($('#mapPopup.show')[0]) {
            isShowMap = true
        }
        else isShowMap = false

        if (!isPageLocked) {
            $('html, body').css({ "overflow": "hidden" })
            isPageLocked = true;
        }
        else if (!isShowMap) {
            $('html, body').css({ "overflow": "visible" })
            isPageLocked = false;
        }
    }


    // landing page
    if ($('.page-landing')[0]) {
        $('#header').toggleClass('header-landing');
    }

    // Drop-down custom
    $('.drop-down__select').click(function () {
        $(this).toggleClass('drop-down__open');
    })


    // ----------------------------------------------------------------------------------------------------//
    // -------------------------------- SearchBox --------------------------------------------------------//
    // --------------------------------------------------------------------------------------------------//
    let toggleSearchBox = function () {
        if (!showSearchbox) {
            let bd = "<div class='modal-backdrop show fade' data-backdrop='static'></div>"
            $(bd).appendTo(document.body);
            showSearchbox = true;
            $('.modal-backdrop').click((e) => {
                $('#searchbox').toggleClass('show')
                toggleSearchBox();
            });
        }
        else if (showSearchbox) {
            $(".modal-backdrop").remove();
            showSearchbox = false;
        }
    }
    $('.search-toggle').click((e) => {
        toggleSearchBox();

    })

    // ----------------------------------------------------------------------------------------------------//
    // -------------------------------- Go to top BTN-----------------------------------------------------//
    // --------------------------------------------------------------------------------------------------//
    $('.gototop').click((e) => {
        e.preventDefault();

        $("html, body").animate({
            scrollTop: 0
        }, {
                duration: ".8s"
            });
    })

    // ----------------------------------------------------------------------------------------------------//
    // --------------------------------------- Megamenu --------------------------------------------------//
    // --------------------------------------------------------------------------------------------------//
    let menuItems = $('.nav-item')
    menuItems.hover(
        function () {
            if (isDesktop) {
                let itemIndex = menuItems.index(this)
                if ($(this).has('.dropdown-menu')[0]) {
                    $(this).toggleClass('show')
                    $(this).find('.dropdown-menu').toggleClass('show')
                }
            }
        })

    // Keep Tablet/Mobile Megamenu in container
    // isDesktopMenu = isDesktop ? true : false;
    let toggleTabletMobileMenu = function () {
        if (isDesktop != !isDesktopMenu) {
            $('#mainnav .nav').toggleClass('container')
            isDesktopMenu = !isDesktopMenu;
        }
    }


    // ----------------------------------------------------------------------------------------------------//
    // -------------------------------- Sticky Megamenu --------------------------------------------------//
    // --------------------------------------------------------------------------------------------------//


    window.addEventListener("scroll", () => {
        let scrollYpos = $(document).scrollTop()
        $('#searchbox').css({ 'top': scrollYpos })
        if (scrollYpos > stickyTrigger && !showsticky) {
            showsticky = true;
            $('.gototop').css({ "opacity": 1 })
            $('#header').toggleClass("header-sticky");
            window.setTimeout(function () {
                $('#header').toggleClass("header-sticky-expanded");
            });
            $('.header-content__logo').attr('src', './newcastle_logo_sticky.png');

        }
        else if (scrollYpos < stickyTrigger && showsticky) {
            showsticky = false;
            $('.gototop').css({ "opacity": 0 })
            $('#header').toggleClass("header-sticky");
            $('#header').toggleClass("header-sticky-expanded");
            $('.header-content__logo').attr('src', './newcastle_logo.png');
        }
    });


    // ----------------------------------------------------------------------------------------------------//
    // -------------------------------- Sticky aside  ----------------------------------------------------//
    // --------------------------------------------------------------------------------------------------//
    let windowWidth;

    console.log("stickyAsideInit - Is anchor present on page:", $("#stickyAsideAnchor").length > 0);

    if ($("#stickyAsideAnchor").length > 0) {

        const asideYpos = $('.aside-location').offset().top;
        const asideHeight = $('.aside-location').height();
        const stickyLimit = $("#stickyAsideAnchor").offset().top;
        window.addEventListener("scroll", () => {
            let scrollYpos = $(document).scrollTop()
            // let asideYoffset = -(asideYpos - 255 - scrollYpos);
            let asideYoffset = scrollYpos;
            // console.log("scrollYpos :", scrollYpos, "asideYpos:", asideYpos, "asideHeight:", asideHeight, "stickyLimit :", stickyLimit, "asideYoffset:", asideYoffset);

            if (isDesktop && scrollYpos > stickyTrigger && (asideYoffset + asideHeight + 300) < stickyLimit) {
                $('.aside-location').css({ "top": asideYoffset - 75 + "px" })
            }
            else if (isDesktop && scrollYpos < stickyTrigger) {
                $('.aside-location').css({ "top": 0 })
            }
        });
    }

    // JS resize event
    $(window).resize(function () {
        checkViewportSize();
        // prevent bug on sticky aside
        // if ($(window).width() == pageWidth) {
        //     $('html, body').animate({
        //         scrollTop: 0
        //     }, 1);
        // }

        // Keep social square aspect ratio
        if ($(window).width() < 768) {
            if ($('.wall-item__social').length > 0) {
                // let first = $('.wall-item__social').eq(0);
                // console.log('social square detected', first.width());
                $('.wall-item__social').eq(0).css({
                    "height": $('.wall-item__social').eq(0).width()
                })
                $('.wall-item__social').eq(1).css({
                    "height": $('.wall-item__social').eq(1).width()
                })
                $('.wall-item__social').eq(2).css({
                    "height": $('.wall-item__social').eq(2).width()
                })
            }
        }
        toggleTabletMobileMenu();


    });


    // ----------------------------------------------------------------------------------------------------//
    // -------------------------------- Vue Components----------------------------------------------------//
    // --------------------------------------------------------------------------------------------------//

    //Listing
    if ($('#listing')[0]) {
        window.vueListing = new Vue({
            el: '#listing',
            data: {
                // listing: [],
                // selectedThing: "all",
                // selectedType: "all",
                // selectedSorting: "popular"
                catList: [],
                classList: [],
                selectedCat: "all",
                selectedClass: "all",
                selectedSorting: "popular",
                listing: [],
                maxItemsToDisplayInit: 12,
                maxItemsToDisplay: null,
                additionalItemsToDisplay: 12
            },
            methods: {
                // sortList(list) {
                //     if (this.selectedSorting == "alpha") {
                //         list.sort(function (a, b) { return a.name.toUpperCase() > b.name.toUpperCase() ? 1 : -1 })
                //     }
                //     if (this.selectedSorting == "popular") {
                //         list.sort(function (a, b) { return a.popularity - b.popularity })
                //     }
                //     return list
                // },
                getListing() {
                    this.maxItemsToDisplay = this.maxItemsToDisplayInit

                    let paramCat = this.selectedCat == 'all' ? '' : this.selectedCat
                    let paramClass = this.selectedClass == 'all' ? '' : this.selectedClass
                    axios.get('https://visitnewcastle.uat.webcoda.net.au/Api/ListApi/GetProducts', {
                        mode: 'no-cors',
                        params: {
                            category: paramCat,
                            classification: paramClass
                        }
                    })
                        .then(response => {
                            console.log("new listing", response.data.listing.length, "items", response.data.listing);
                            this.listing = response.data.listing
                            this.sortListing()
                        })
                        .catch(error => {
                            console.log(error);
                        })
                },
                loadMoreItems() {
                    this.maxItemsToDisplay = this.maxItemsToDisplay + this.additionalItemsToDisplay

                },
                togglePagelock(e) {
                    $(function () {
                        togglePageLock()
                    })
                },
                blur(e) {
                    e.target.blur()
                },
                sortListing() {
                    if (this.selectedSorting == "popular") {
                        this.listing.sort(function (a, b) { return b.popularity - a.popularity })
                    }
                    else if (this.selectedSorting == "alpha") {
                        this.listing.sort(function (a, b) { return a.name.toUpperCase() > b.name.toUpperCase() ? 1 : -1 })
                    }
                    populateMap(this.listing)
                }
            },
            computed: {
                filteredListing: function () {
                    return this.listing.slice(0, this.maxItemsToDisplay)
                },
                isMoreItems: function () {
                    return this.listing.length > this.maxItemsToDisplay
                }
                // filteredListing: function () {
                //     let list = []
                //     if (this.selectedThing != "all" && this.selectedType != "all") {
                //         list = this.listing.filter(item => item.categoryId == this.selectedThing).filter(item => item.classifications == this.selectedType)
                //     }
                //     else if (this.selectedThing != "all") list = this.listing.filter(item => item.categoryId == this.selectedThing)

                //     if (this.selectedType != "all" && this.selectedThing != "all") {
                //         list = this.listing.filter(item => item.classifications == this.selectedType).filter(item => item.categoryId == this.selectedThing)
                //     }
                //     else if (this.selectedType != "all") list = this.listing.filter(item => item.classifications == this.selectedType)

                //     if (this.selectedType == "all" && this.selectedThing == "all") return this.sortList(this.listing)
                //     else return this.sortList(list)
                // },
                // themeList: function () {
                //     return new Set(this.listing.map(item => item.categoryId))
                // },
                // typeList: function () {
                //     return new Set(this.listing.map(item => item.classifications))
                // }
            },
            watch: {
                selectedCat() {
                    this.selectedClass = 'all'

                    axios.get('https://visitnewcastle.uat.webcoda.net.au/Api/ListApi/GetClassifications', {
                        mode: 'no-cors',
                        params: {
                            category: this.selectedCat
                        }
                    })
                        .then(response => {
                            this.classList = response.data
                        })
                        .catch(error => {
                            console.log(error);
                        })
                    this.getListing()
                },
                selectedClass() {
                    this.getListing()
                },
                selectedSorting() {
                    this.sortListing()
                }
                // filteredListing(newValue, oldValue) {
                //     console.log("WATCH filteredListing : ", newValue);
                //     if (isGoogleMapReady) {
                //         populateMap(newValue)
                //     }
                //     // if (newValue != oldValue) {
                //     //     console.log("populateMap from Vue");
                //     //     populateMap(this.filteredListing)
                //     // }
                // }
            },
            mounted: function () {
                axios.get('https://visitnewcastle.uat.webcoda.net.au/Api/ListApi/getcategories', {
                    mode: 'no-cors'
                })
                    .then(response => {
                        this.catList = response.data
                    })
                    .catch(error => {
                        console.log(error);
                    })
                this.getListing()
                // // https://visitnewcastlefrontend.uat.webcoda.net.au/
                // axios.get('http://192.168.0.106:1234/' + 'things.json', {
                //     params: {
                //         null: null
                //     }
                // })
                //     .then(res => {
                //         this.listing = res.data.listing
                //         console.log("---> JSON on mounted!");

                //         // if (isGoogleMapReady) console.log("JSON and GoogleMpsReady!");
                //         // console.log("populateMap from Vue");
                //         // populateMap(this.filteredListing)
                //     })
                //     .catch(error => {
                //         console.log(error);
                //     })
            }
        })
    }

    // Carousel
    $('#carouselWhatson').on('slide.bs.carousel', function (e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 4;
        var totalItems = $('#carouselWhatson .carousel-item').length;

        if (idx >= totalItems - (itemsPerSlide - 1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i = 0; i < it; i++) {
                // append slides to end
                if (e.direction == "left") {
                    $('#carouselWhatson .carousel-item').eq(i).appendTo('#carouselWhatson .carousel-inner');
                }
                else {
                    $('#carouselWhatson .carousel-item').eq(0).appendTo('#carouselWhatson .carousel-inner');
                }
            }
        }
    })
    $('#carouselFromBlog').on('slide.bs.carousel', function (e) {
        console.log('Click on carousel');

        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 4;
        var totalItems = $('#carouselWhatson .carousel-item').length;

        if (idx >= totalItems - (itemsPerSlide - 1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i = 0; i < it; i++) {
                // append slides to end
                if (e.direction == "left") {
                    $('#carouselFromBlog .carousel-item').eq(i).appendTo('#carouselFromBlog .carousel-inner');
                }
                else {
                    $('#carouselFromBlog .carousel-item').eq(0).appendTo('#carouselFromBlog .carousel-inner');
                }
            }
        }
    })

    // social-feed
    if ($('#socialFeed')[0]) {
        console.log("Vue component detected");

        new Vue({
            el: '#socialFeed',
            data: {
                isInstaFeed: true,
                isFbFeed: false,
                instaFeed: [],
                fbFeed: []
            },
            methods: {
                prevSocialItem() {
                    if (this.isInstaFeed) this.instaFeed.unshift(this.instaFeed.pop())
                    if (this.isFbFeed) this.fbFeed.unshift(this.fbFeed.pop())
                },
                nextSocialItem() {
                    if (this.isInstaFeed) this.instaFeed.push(this.instaFeed.shift())
                    if (this.isFbFeed) this.fbFeed.push(this.fbFeed.shift())
                },
                switchFeed(which) {
                    switch (which) {
                        case "insta":
                            this.isInstaFeed = true;
                            this.isFbFeed = false;
                            break
                        case "fb":
                            this.isInstaFeed = false;
                            this.isFbFeed = true;
                            break
                        default:
                            this.isInstaFeed = true;
                            this.isFbFeed = false;
                    }

                }
            },
            mounted() {
                axios.get('https://www.instagram.com/citynewcastle.au/', {
                    params: {
                        __a: 1
                    }
                })
                    .then(response => {
                        this.instaFeed = response.data.graphql.user.edge_owner_to_timeline_media.edges;
                    })
                    .catch(error => {
                        console.log(error);
                    })

                axios.get('https://graph.facebook.com/v3.3/visitnewcastle', {
                    params: {
                        fields: 'feed{created_time,attachments,message}',
                        access_token: 'EAAE17CbQjxMBADus0kBJvzkRpMKnY3PVtI1R91Eei44eEEZBFkAs2hlVvYOrc6oD03RXV4442OMtZAXAnMtAtqXiA2SbY5FvKfgoFdtOOCcArN4WwyMEsrHSzMMFm1LQNv4csFk6PGvbtHGMayUZBXtBxvpFLiwbQvr7ZCz5XAZDZD'
                    }
                })
                    .then(response => {
                        // this.fbFeed = response.data.feed.data
                        // console.log(this.fbFeed);

                        for (const item of response.data.feed.data) {
                            this.fbFeed.push({ "image": item.attachments.data[0].media.image.src, "legend": item.attachments.data[0].description })
                        }
                    })
                    .catch(error => {
                        console.log(error);
                    })

                // fetch('https://graph.facebook.com/v3.3/visitnewcastle?fields=feed{created_time,attachments,message}&access_token=EAAE17CbQjxMBADus0kBJvzkRpMKnY3PVtI1R91Eei44eEEZBFkAs2hlVvYOrc6oD03RXV4442OMtZAXAnMtAtqXiA2SbY5FvKfgoFdtOOCcArN4WwyMEsrHSzMMFm1LQNv4csFk6PGvbtHGMayUZBXtBxvpFLiwbQvr7ZCz5XAZDZD')
                //     .then(res => res.json())
                //     .then(data => {
                //         console.log(JSON.stringify(data.feed.data[0].attachments.data[0].media.image.src))

                //         this.fbFeed = data.feed.data
                //         console.log("this.fbFeed:", this.fbFeed);
                //     })

            }
        })
    }


    // REMOVE FOR PROD !! -------------------------------------\/

    // (function (d, t) {
    //     console.log('BUG Herd Init', d, t);
    //     var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
    //     console.log('BUG Herd debug:', bh);
    //     bh.type = 'text/javascript';
    //     bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=jbmkes8xkkqknvxsbpqutg';
    //     s.parentNode.insertBefore(bh, s);
    // })(document, 'script');

    // REMOVE FOR PROD !! -------------------------------------/\


});